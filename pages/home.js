module.exports = {
    elements: {
        loginName: {
            selector: 'input[name=email]'
        },
        loginPassword: {
            selector: 'input[name=password]'
        },
        loginSubmit: {
            selector: 'input[name=submit]'
        }
    }
};

