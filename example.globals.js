module.exports = {
    host_a: {
        host: 'fallen-tower.tvrn.dev',
        name: 'nick',
        email: 'nick@fallen-tower.tvrn.dev',
        password: 'password',
        actor: 'nick@fallen-tower.tvrn.dev'
    },
    host_b: {
        host: 'flagon-dragon.tvrn.dev',
        name: 'mattie',
        email: 'mattie@flagon-dragon.tvrn.dev',
        password: 'password',
        actor: 'mattie@flagon-dragon.tvrn.dev'
    },
    host_c: {
        host: 'elfsong.tvrn.dev',
        name: 'mason',
        email: 'mason@elfsong.tvrn.dev',
        password: 'password',
        actor: 'mason@elfsong.tvrn.dev'
    },
    host_d: {
        host: 'oak-and-spear.tvrn.dev',
        name: 'vanessa',
        email: 'vanessa@oak-and-spear.tvrn.dev',
        password: 'password',
        actor: 'vanessa@oak-and-spear.tvrn.dev'
    },
}