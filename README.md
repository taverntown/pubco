# pubco

Pubco contains tools and scripts to automate the testing of Tavern via [nightwatchjs](https://nightwatchjs.org).

This test suite is organized by major feature group:

* Site - authenticate, view settings, change settings
* Network - follow, accept, reject, unfollow
* Publish - create note, reply to note, announce note, recent feed, my feed, local feed
* Group - create, join, leave, invite, uninvite

Because a lot of functionality is federated server-to-server functionality, the test suite requires 5 different endpoints to operate against.

# Usage

1. Create an environment using `tavern skeleton`:

   `$ mkdir ./environment`
   `$ tavern skeleton --destination ./environment/ --web-name fallen_tower --web-name flagon_dragon --web-name elfsong --web-name oak_and_spear --web-name cointoss --domain fallen-tower.tvrn.dev --domain flagon-dragon.tvrn.dev --domain elfsong.tvrn.dev --domain oak-and-spear.tvrn.dev --domain cointoss.tvrn.dev --web-port 9500 --web-user nick --web-user mattie --web-user mason --web-user vanessa --web-user hannah`

2. Bootstrap and run the development cluster.

   `$ ./environment && ./run.sh`

3. Configure globals and run the test suite.

   `$ cp example.globals.js globals.js`
   `$ vim globals.js`
   `$ nightwatch`

## Ngrok

The easiest way to run the test suite is to run your cluster locally with ngrok configured cnames.

1. On ngrok, create a handful of cname references for the domains that you want to use.
2. Using `certbot` create a lets-encrypt certificate for your endpoints.
3. Run ngrok with configuration that looks something like this:

```yaml
tunnels:
  fallen-tower:
    addr: 9500
    proto: tls
    inspect: false
    hostname: fallen-tower.tvrn.dev
    host_header: fallen-tower.tvrn.dev
    crt: /path/to/fullchain.pem
    key: /path/to/privkey.pem
  flagon-dragon:
    addr: 9501
    proto: tls
    inspect: false
    hostname: flagon-dragon.tvrn.dev
    host_header: flagon-dragon.tvrn.dev
    crt: /path/to/fullchain.pem
    key: /path/to/privkey.pem
  elfsong.tvrn.dev:
    addr: 9502
    proto: tls
    inspect: false
    hostname: elfsong.tvrn.dev
    host_header: elfsong.tvrn.dev
    crt: /path/to/fullchain.pem
    key: /path/to/privkey.pem
```