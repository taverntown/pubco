describe('Compose', function () {

    test('Note', (browser) => {
        browser
            .deleteCookies();

        // user a
        // - create note
        // - verify feed
        // user b
        // - verify feed empty
        // - follow user a
        // user a
        // - verify follower
        // - create note
        // user b
        // - verify feed not empty

        browser
            // [user a] login
            .url(browser.globals.url('host_a'))
            .setValue('input[name=email]', browser.globals.host_a.email)
            .setValue('input[name=password]', browser.globals.host_a.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_a') + 'feed', 'Site url matches')
            // [user a] create post #1
            .click({locateStrategy: 'xpath', selector: '//li[@id="navcompose"]/a'})
            .assert.urlEquals(browser.globals.url('host_a') + 'compose')
            .setValue('input[name=content]', 'Hello World')
            .click('input[name=submit]');

        browser.pause(1000);

        browser
            // [user b] login
            .url(browser.globals.url('host_b'))
            .setValue('input[name=email]', browser.globals.host_b.email)
            .setValue('input[name=password]', browser.globals.host_b.password)
            .click('input[name=submit]')

            // [user b] verify empty feed
            .assert.containsText('body', 'Nothing to display.')

            // [user b] follow user a
            .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'})
            .setValue('input[name=actor]', '@' + browser.globals.host_a.actor)
            .click('input[name=submit]');

        browser.pause(3000);

        browser
            // [user a] verify follower
            .url(browser.globals.url('host_a') + 'manage/network')
            .perform(function () {
                browser.expect.elements('tr.follower').count.to.equal(1);
                browser.assert.containsText('tr.follower', 'https://' + browser.globals.host_b.host + '/users/' + browser.globals.host_b.name);
            })

            // [user a] create post #2
            .url(browser.globals.url('host_a') + 'feed')
            .click({locateStrategy: 'xpath', selector: '//li[@id="navcompose"]/a'})
            .assert.urlEquals(browser.globals.url('host_a') + 'compose')
            .setValue('input[name=content]', 'c021f0efb19a2d09ce987f97e12cbc33')
            .click('input[name=submit]');

        browser.pause(3000);

        browser
            // [user b] verify feed
            .url(browser.globals.url('host_b') + 'feed')
            .perform(function () {
                browser.assert.containsText('#feed', 'c021f0efb19a2d09ce987f97e12cbc33');
            });
    });

    test('Boost', (browser) => {
        browser
            .deleteCookies();

        // user c
        // - verify feed empty
        // - follow user b
        // user b
        // - verify follower
        // - boost user a post #2
        // user c
        // - verify feed not empty

        browser
            // [user c] login
            .url(browser.globals.url('host_c'))
            .setValue('input[name=email]', browser.globals.host_c.email)
            .setValue('input[name=password]', browser.globals.host_c.password)
            .click('input[name=submit]')

            // [user c] verify empty feed
            .assert.containsText('body', 'Nothing to display.')

            // [user c] follow user a
            .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'})
            .setValue('input[name=actor]', '@' + browser.globals.host_b.actor)
            .click('input[name=submit]');

        browser.pause(3000);

        browser
            // [user b] login
            .url(browser.globals.url('host_b'))
            .setValue('input[name=email]', browser.globals.host_b.email)
            .setValue('input[name=password]', browser.globals.host_b.password)
            .click('input[name=submit]')
            // [user b] verify follower
            .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'})
            .perform(function () {
                browser.expect.elements('tr.follower').count.to.equal(1);
                browser.assert.containsText('tr.follower', 'https://' + browser.globals.host_c.host + '/users/' + browser.globals.host_c.name);
            })

            // [user b] boost post
            .url(browser.globals.url('host_b') + 'feed')
            .perform(function () {
                browser.assert.containsText('#feed', 'c021f0efb19a2d09ce987f97e12cbc33');
            })
            .click({
                locateStrategy: 'xpath',
                selector: '//div[contains(@class, "tavern-feed-item")]//button[contains(@class, "tavern-feed-item-menu")]'
            })
            .click({
                locateStrategy: 'xpath',
                selector: '//div[contains(@class, "tavern-feed-item")]//a[contains(@class, "announce")]'
            });

        browser.pause(3000);

        browser
            // [user c] verify feed
            .url(browser.globals.url('host_c') + 'feed')
            .perform(function () {
                browser.assert.containsText('#feed', 'c021f0efb19a2d09ce987f97e12cbc33');
            });
    });

});