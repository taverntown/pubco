describe('Network', function () {

    test('Auto Follow', (browser) => {
        browser
            .deleteCookies();

        browser
            .url(browser.globals.url('host_a'))
            .assert.containsText('.navbar-brand', 'Fallen Tower', 'Site name matches')
            .setValue('input[name=email]', browser.globals.host_a.email)
            .setValue('input[name=password]', browser.globals.host_a.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_a') + 'feed', 'Site url matches')
            .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'})
            .assert.urlEquals(browser.globals.url('host_a') + 'manage/network')
            .setValue('input[name=actor]', '@' + browser.globals.host_b.actor)
            .click('input[name=submit]', function () {
                browser.pause(1000);
            })
            .assert.containsText('table', 'https://' + browser.globals.host_b.host + '/users/' + browser.globals.host_b.name);

        browser
            .url(browser.globals.url('host_b'))
            .assert.containsText('.navbar-brand', 'Flagon Dragon', 'Site name matches')
            .setValue('input[name=email]', browser.globals.host_b.email)
            .setValue('input[name=password]', browser.globals.host_b.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_b') + 'feed', 'Site url matches')
            .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'})
            .assert.urlEquals(browser.globals.url('host_b') + 'manage/network')
            .assert.containsText('body', 'https://' + browser.globals.host_a.host + '/users/' + browser.globals.host_a.name);

        browser.end();
    });

    describe('Manual Follow', function () {
        test('Accept', (browser) => {
            browser
                .deleteCookies();

            browser
                // login
                .url(browser.globals.url('host_c'))
                .setValue('input[name=email]', browser.globals.host_c.email)
                .setValue('input[name=password]', browser.globals.host_c.password)
                .click('input[name=submit]')
                .assert.containsText('h1', 'Feed')

                // visit configuration
                .click({locateStrategy: 'xpath', selector: '//li/a[text()="Configure"]'})
                .assert.urlEquals(browser.globals.url('host_c') + 'configure')
                .assert.attributeEquals('#userAutoAcceptFollowers', 'checked', 'true')
                .click('input[id=userAutoAcceptFollowers]')
                .click('input[name=submit]', function () {
                    browser.pause(1000);
                    // verify configuration
                    browser.assert.urlEquals(browser.globals.url('host_c') + 'configure');
                    browser.expect.element('#userAutoAcceptFollowers').to.not.have.attribute('checked-attr');
                })
                .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'})
                .assert.urlEquals(browser.globals.url('host_c') + 'manage/network')
                .perform(function () {
                    // verify configuration
                    browser.expect.elements('tr.pending-follower').count.to.equal(0);
                    browser.expect.elements('tr.follower').count.to.equal(0);
                });

            browser
                // login
                .url(browser.globals.url('host_d'))
                .setValue('input[name=email]', browser.globals.host_d.email)
                .setValue('input[name=password]', browser.globals.host_d.password)
                .click('input[name=submit]')
                // visit network and follow
                .click({selector: '//li/a[text()="Network"]', locateStrategy: 'xpath'})
                .setValue('input[name=actor]', '@' + browser.globals.host_c.actor)
                .click('input[name=submit]', function () {
                    browser.pause(5000);
                    browser.expect.elements('tr.pending-following').count.to.equal(1);
                    browser.assert.containsText('tr.pending-following', 'https://' + browser.globals.host_c.host + '/users/' + browser.globals.host_c.name);
                });

            browser
                .url(browser.globals.url('host_c') + 'manage/network')
                .perform(function () {
                    // verify pending follower
                    browser.expect.elements('tr.pending-follower').count.to.equal(1);
                    browser.assert.containsText('tr.pending-follower', 'https://' + browser.globals.host_d.host + '/users/' + browser.globals.host_d.name);
                })
                .click({
                    locateStrategy: 'xpath',
                    selector: '//tr[@class="pending-follower"]//input[@value="Accept"]'
                }, function () {
                    browser.pause(5000);
                    browser.expect.elements('tr.pending-follower').count.to.equal(0);
                    browser.expect.elements('tr.follower').count.to.equal(1);
                    browser.assert.containsText('tr.follower', 'https://' + browser.globals.host_d.host + '/users/' + browser.globals.host_d.name);
                });

            browser
                .url(browser.globals.url('host_d') + "manage/network")
                .perform(function () {
                    browser.expect.elements('tr.following').count.to.equal(1);
                    browser.expect.elements('tr.pending-following').count.to.equal(0);
                    browser.assert.containsText('tr.following', 'https://' + browser.globals.host_c.host + '/users/' + browser.globals.host_c.name);
                });

            browser
                .url(browser.globals.url('host_c') + "manage/network")
                .perform(function () {
                    browser.expect.elements('tr.pending-follower').count.to.equal(0);
                    browser.expect.elements('tr.follower').count.to.equal(1);
                    browser.assert.containsText('tr.follower', 'https://' + browser.globals.host_d.host + '/users/' + browser.globals.host_d.name);
                });

            browser.end();
        });


        test('Reject', (browser) => {
            browser
                .deleteCookies();

            browser
                // login
                .url(browser.globals.url('host_c'))
                .setValue('input[name=email]', browser.globals.host_c.email)
                .setValue('input[name=password]', browser.globals.host_c.password)
                .click('input[name=submit]')
                .assert.containsText('h1', 'Feed')
                .click({locateStrategy: 'xpath', selector: '//li/a[text()="Configure"]'}, function () {
                    browser.assert.urlEquals(browser.globals.url('host_c') + 'configure');
                    browser.expect.element('#userAutoAcceptFollowers').to.not.have.attribute('checked-attr');
                })
                .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'}, function () {
                    browser.assert.urlEquals(browser.globals.url('host_c') + 'manage/network');
                    browser.expect.elements('tr.pending-follower').count.to.equal(0);
                });

            browser
                // login
                .url(browser.globals.url('host_e'))
                .setValue('input[name=email]', browser.globals.host_e.email)
                .setValue('input[name=password]', browser.globals.host_e.password)
                .click('input[name=submit]')
                .assert.containsText('h1', 'Feed')

                // visit network and follow
                .click({selector: '//li/a[text()="Network"]', locateStrategy: 'xpath'})
                .setValue('input[name=actor]', '@' + browser.globals.host_c.actor)
                .click('input[name=submit]', function () {
                    browser.expect.elements('tr.pending-following').count.to.equal(1);
                    browser.assert.containsText('tr.pending-following', 'https://' + browser.globals.host_c.host + '/users/' + browser.globals.host_c.name);
                });

            browser.pause(5000);

            browser
                .url(browser.globals.url('host_c') + "manage/network")
                .perform(function () {
                    browser.expect.elements('tr.pending-follower').count.to.equal(1);
                    browser.assert.containsText('tr.pending-follower', 'https://' + browser.globals.host_e.host + '/users/' + browser.globals.host_e.name);
                })
                .click({
                    locateStrategy: 'xpath',
                    selector: '//tr[@class="pending-follower"]//input[@value="Reject"]'
                }, function () {
                    browser.expect.elements('tr.pending-follower').count.to.equal(0);
                });

            browser.pause(5000);

            browser
                .url(browser.globals.url('host_e') + "manage/network")
                .perform(function () {
                    browser.expect.elements('tr.following').count.to.equal(0);
                    browser.expect.elements('tr.pending-following').count.to.equal(0);
                });

            browser.end();
        });
    });

    test('Undo Follow', (browser) => {
        browser
            .deleteCookies();

        browser
            .url(browser.globals.url('host_a'))
            .setValue('input[name=email]', browser.globals.host_a.email)
            .setValue('input[name=password]', browser.globals.host_a.password)
            .click('input[name=submit]')
            .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'})
            .assert.urlEquals(browser.globals.url('host_a') + 'manage/network')
            .perform(function () {
                browser.expect.elements('tr.following').count.to.equal(1);
                browser.assert.containsText('tr.following', 'https://' + browser.globals.host_b.host + '/users/' + browser.globals.host_b.name);
            });

        browser
            .url(browser.globals.url('host_b'))
            .setValue('input[name=email]', browser.globals.host_b.email)
            .setValue('input[name=password]', browser.globals.host_b.password)
            .click('input[name=submit]')
            .click({locateStrategy: 'xpath', selector: '//li/a[text()="Network"]'})
            .assert.urlEquals(browser.globals.url('host_b') + 'manage/network')
            .perform(function () {
                browser.expect.elements('tr.follower').count.to.equal(1);
                browser.assert.containsText('tr.follower', 'https://' + browser.globals.host_a.host + '/users/' + browser.globals.host_a.name);
            });

        browser
            .url(browser.globals.url('host_a') + 'manage/network')
            .click({
                locateStrategy: 'xpath',
                selector: '//tr[@class="following"]//input[@value="Unfollow"]'
            }, function () {
                browser.expect.elements('tr.following').count.to.equal(0);
            });

        browser.pause(5000);

        browser
            .url(browser.globals.url('host_b') + 'manage/network')
            .perform(function () {
                browser.expect.elements('tr.follower').count.to.equal(0);
            });

        browser.end();
    });

    after(browser => browser.end());

});