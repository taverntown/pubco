describe('Site', function () {
    test('authentication', (browser) => {
        browser
            .deleteCookies();

        browser
            .url(browser.globals.url('host_a'))
            .assert.containsText('.navbar-brand', 'Fallen Tower', 'Site name matches')
            .setValue('input[name=email]', browser.globals.host_a.email)
            .setValue('input[name=password]', browser.globals.host_a.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_a') + 'feed', 'Site url matches');

        browser
            .url(browser.globals.url('host_b'))
            .assert.containsText('.navbar-brand', 'Flagon Dragon', 'Site name matches')
            .setValue('input[name=email]', browser.globals.host_b.email)
            .setValue('input[name=password]', browser.globals.host_b.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_b') + 'feed', 'Site url matches');

        browser
            .url(browser.globals.url('host_c'))
            .assert.containsText('.navbar-brand', 'Elfsong', 'Site name matches')
            .setValue('input[name=email]', browser.globals.host_c.email)
            .setValue('input[name=password]', browser.globals.host_c.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_c') + 'feed', 'Site url matches');

        browser
            .url(browser.globals.url('host_d'))
            .assert.containsText('.navbar-brand', 'Oak and Spear', 'Site name matches')
            .setValue('input[name=email]', browser.globals.host_d.email)
            .setValue('input[name=password]', browser.globals.host_d.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_d') + 'feed', 'Site url matches');

        browser
            .url(browser.globals.url('host_e'))
            .assert.containsText('.navbar-brand', 'Cointoss', 'Site name matches')
            .setValue('input[name=email]', browser.globals.host_e.email)
            .setValue('input[name=password]', browser.globals.host_e.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_e') + 'feed', 'Site url matches');

        browser.end();
    });

    test('browse logged in pages', (browser) => {
        browser
            .deleteCookies();

        browser
            .url(browser.globals.url('host_a'))
            .assert.containsText('.navbar-brand', 'Fallen Tower', 'Site name matches')
            .setValue('input[name=email]', browser.globals.host_a.email)
            .setValue('input[name=password]', browser.globals.host_a.password)
            .click('input[name=submit]')
            .assert.urlEquals(browser.globals.url('host_a') + 'feed', 'Site url matches')

            .click({locateStrategy: 'xpath', selector: '//div[@id="tavernNav"]/ul/li/a[text()="Configure"]'})
            .assert.urlEquals(browser.globals.url('host_a') + 'configure', 'Can navigate to configuration')

            .click({locateStrategy: 'xpath', selector: '//div[@id="tavernNav"]/ul/li/a[text()="Network"]'})
            .assert.urlEquals(browser.globals.url('host_a') + 'manage/network', 'Can navigate to manage network')

            .click({locateStrategy: 'xpath', selector: '//div[@id="tavernNav"]/ul/li/a[text()="Groups"]'})
            .assert.urlEquals(browser.globals.url('host_a') + 'manage/groups', 'Can navigate to manage groups')

            .click({locateStrategy: 'xpath', selector: '//div[@id="tavernNav"]/ul/li/a[text()="Profile"]'})
            .assert.urlEquals(browser.globals.url('host_a') + 'users/' + browser.globals.host_a.name, 'Can navigate to my profile page')

            .click({locateStrategy: 'xpath', selector: '//div[@id="tavernNav"]/ul/li/a[text()="Feed"]'})
            .assert.urlEquals(browser.globals.url('host_a') + 'feed', 'Can navigate to the main feed');

        browser.end();
    });

    after(browser => browser.end());
});
